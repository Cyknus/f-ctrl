Hello.

First of all you must set the following env variables.

```
FLASK_ENV=development
FLASK_APP=f_control
DBURL=postgresql://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME
```

As you can see, you need to install (or run a container with) the best 
object-relational database managment system. This README won't explain how to.

Next, you need to clone the repo. I won't explain that either.

Next, *inside your virtualenv* run `pip install -r requirements.txt`

Next, you can run the app with `flask run` this is needed for flask to
run the migration and create the needed tables. I have not written a 
`flask migrate`. However, it is needed for the next step.

Next, run `python`. Once on interactive Python do the following:
```
>>> from werkzeug.security import generate_password_hash
>>> generate_password_hash('myinsecurepassword')
'pbkdf2:sha256:150000$H6wnm78t$f234561d6a7ba15d7a90c65b7ee3538932239b79139bb50f8d37630aace5ac28'
>>>
```
You will need to create a new record into the users table on you db with 
your desired username and the result of `generate_password_hash` as password.
This is because every section of f^ is login_required but there is no signing up
functionality atm.

After that, you can run the flask server and log in to access to all its glory.
Or develop the tickets that you were assigned, whatever.

Have a good day.