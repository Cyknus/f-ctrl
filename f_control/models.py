from datetime import datetime, timedelta, date
from functools import reduce
from math import ceil

from flask_sqlalchemy import SQLAlchemy as SQLA
from werkzeug.security import generate_password_hash

db = SQLA()


class Persistent:
    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class User(db.Model, Persistent):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    password = db.Column(db.String)

    def __init__(self, name, password):
        self.name = name
        self.password = generate_password_hash(password)

    def __repr__(self):
        return "<User {0}, {1}>".format(self.id, self.name)


class Wallet(db.Model, Persistent):
    __tablename__ = "wallet"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    balance = db.Column(db.Float)
    acct_type = db.Column(db.String)
    updated_at = db.Column(db.DateTime(timezone=True))

    def __init__(self, name, balance, acct_type, updated_at):
        self.name = name
        self.balance = balance
        self.acct_type = acct_type
        self.updated_at = updated_at

    def __repr__(self):
        return "<Wallet {0}, {1}>".format(self.id, self.name)

    def debit(self, amount):
        if self.acct_type == "credit":
            self.balance -= amount
        else:
            self.balance += amount

        self.updated_at = datetime.now()

        self.save()

    def credit(self, amount):
        if self.acct_type == "credit":
            self.balance += amount
        else:
            self.balance -= amount

        self.updated_at = datetime.now()

        self.save()


class Debit(db.Model, Persistent):
    __tablename__ = "debit"

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float)
    concept = db.Column(db.String)
    recurrence = db.Column(db.String)
    wallet_id = db.Column(db.Integer, db.ForeignKey(Wallet.id))
    created_at = db.Column(db.DateTime(timezone=True))

    wallet = db.relationship("Wallet", foreign_keys="Debit.wallet_id")

    def __init__(self, amount, concept, recurrence, created_at, wallet_id, **kwargs):
        self.amount = amount
        self.concept = concept
        self.recurrence = recurrence
        self.created_at = created_at
        self.wallet_id = wallet_id

    def __repr__(self):
        return "<Debit {0}, {1}>".format(self.id, self.amount)


class Credit(db.Model, Persistent):
    __tablename__ = "credit"

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float)
    concept = db.Column(db.String)
    recurrence = db.Column(db.String)
    subs = db.Column(db.String)
    hidden = db.Column(db.Integer)
    msi = db.Column(db.Integer)
    store = db.Column(db.String)
    feeling = db.Column(db.String)
    wallet_id = db.Column(db.Integer, db.ForeignKey(Wallet.id))
    created_at = db.Column(db.DateTime(timezone=True))

    wallet = db.relationship("Wallet", foreign_keys="Credit.wallet_id")

    def __init__(
        self,
        amount,
        concept,
        recurrence,
        subs,
        hidden,
        msi,
        store,
        feeling,
        created_at,
        wallet_id,
        **kwargs
    ):
        self.amount = amount
        self.concept = concept
        self.recurrence = recurrence
        self.created_at = created_at
        self.subs = subs
        self.hidden = hidden
        self.msi = msi
        self.store = store
        self.feeling = feeling
        self.wallet_id = wallet_id

    def __repr__(self):
        return "<Credit {0}, {1}>".format(self.id, self.amount)

    def is_active_on(self: object, given_dt: datetime) -> bool:
        creation_month = self.created_at.month
        end_month = creation_month + self.msi
        end_year = self.created_at.year

        if end_month > 12:
            end_year += end_month // 12
            end_month %= 12

        end = self.created_at.replace(month=end_month, year=end_year, tzinfo=None)
        return end > given_dt


class Cash(db.Model, Persistent):
    __tablename__ = "cash"

    id = db.Column(db.Integer, primary_key=True)
    wallet_id = db.Column(db.Integer, db.ForeignKey(Wallet.id))

    wallet = db.relationship("Wallet", foreign_keys="Cash.wallet_id")

    def __init__(self, wallet_id):
        self.wallet_id = wallet_id


class DebitAcct(db.Model, Persistent):
    __tablename__ = "debit_acct"

    id = db.Column(db.Integer, primary_key=True)
    is_savings = db.Column(db.Integer)
    interest_rate = db.Column(db.Float)
    wallet_id = db.Column(db.Integer, db.ForeignKey(Wallet.id))

    wallet = db.relationship("Wallet", foreign_keys="DebitAcct.wallet_id")

    def __init__(self, is_savings, interest_rate, wallet_id):
        self.is_savings = is_savings
        self.interest_rate = interest_rate
        self.wallet_id = wallet_id


class CreditAcct(db.Model, Persistent):
    __tablename__ = "credit_acct"

    id = db.Column(db.Integer, primary_key=True)
    closure_day = db.Column(db.Integer)
    interest_rate = db.Column(db.Float)
    credit_limit = db.Column(db.Float)
    yearly_fee = db.Column(db.Float)
    yf_date = db.Column(db.DateTime(timezone=True))

    wallet_id = db.Column(db.Integer, db.ForeignKey(Wallet.id))
    wallet = db.relationship("Wallet", foreign_keys="CreditAcct.wallet_id")

    def __init__(
        self, closure_day, credit_limit, interest_rate, yearly_fee, yf_date, wallet_id
    ):
        self.closure_day = closure_day
        self.credit_limit = credit_limit
        self.interest_rate = interest_rate
        self.yearly_fee = yearly_fee
        self.yf_date = yf_date
        self.wallet_id = wallet_id

    def next_payment(self) -> float:
        prev_closure_day = self.previous_closure_day()
        regular_charges = Credit.query.filter(
            Credit.created_at > prev_closure_day,
            self.wallet_id == Credit.wallet_id,
            Credit.msi == 1,
        ).all()
        msi_charges = Credit.query.filter(
            Credit.msi > 1, self.wallet_id == Credit.wallet_id
        ).all()
        next_closure_day = self.next_closure_day()
        active_msi = [
            msi.amount / msi.msi
            for msi in msi_charges
            if msi.is_active_on(next_closure_day)
        ]
        regular_charges = map(lambda a: a.amount, regular_charges)

        total = reduce(lambda a, b: a + b, active_msi, 0)
        total += reduce(lambda a, b: a + b, regular_charges, 0)
        return total

    def next_closure_day(self) -> datetime:
        prev_closure_day = self.previous_closure_day()
        next_month = prev_closure_day.month + 1
        next_month = 1 if next_month == 13 else next_month
        next_closure_day = prev_closure_day.replace(month=next_month)
        return next_closure_day

    def previous_closure_day(self) -> datetime:
        zero = {"hour": 0, "minute": 0}
        today = datetime.today()
        today = today.replace(**zero)
        prev = today.replace(day=self.closure_day + 1)
        if prev > today:
            last_month = today.month - 1
            last_month = last_month if last_month > 0 else 12
            prev = prev.replace(month=last_month)

        return prev


class Subcategory(db.Model, Persistent):
    __tablename__ = "subcategory"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    details = db.Column(db.String)
    category = db.Column(db.String)

    def __init__(self, name, details, category):
        self.name = name
        self.details = details
        self.category = category


class DailyBalance(db.Model, Persistent):
    __table_name__ = "daily_balance"

    id = db.Column(db.Integer, primary_key=True)
    balance = db.Column(db.Float)
    day = db.Column(db.Date)
    credit_acct_id = db.Column(db.Integer, db.ForeignKey(CreditAcct.id))
    credit_acct = db.relationship(
        "CreditAcct", foreign_keys="DailyBalance.credit_acct_id"
    )

    def __init__(self, balance, credit_acct_id, day=date.today()):
        self.balance = balance
        self.day = day
        self.credit_acct_id = credit_acct_id
