from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for,
    session,
    current_app,
)

from f_control.auth import login_required
from f_control.views.charges import record_charge, get_charge_opts
from f_control.views.debits import record_debit
from f_control.views.wallets import add_wallet, get_accts
from f_control.views.balances import balance_overview
from f_control.models import (
    User,
    Wallet,
    Subcategory,
    Credit,
    Debit,
    Cash,
    CreditAcct,
    DebitAcct,
)

bp = Blueprint("wallet", __name__)


@bp.route("/register")
def register():
    u = User(666, "uriel", "password")
    u.save()
    return "OK"


@bp.route("/")
def index():
    return render_template("wallet/index.html")


@bp.route("/charge", methods=("GET", "POST"))
@login_required
def charge():
    saved = False
    opts = get_charge_opts()
    if request.method == "GET":
        return render_template("wallet/charge.html", **opts)

    saved = record_charge(request.form)

    if "continue" not in request.form:
        return render_template("wallet/index.html", charge_saved=saved)

    return render_template("wallet/charge.html", charge_saved=saved, **opts)


@bp.route("/debit", methods=("GET", "POST"))
@login_required
def debit():
    wallets = Wallet.query.all()
    if request.method == "GET":
        return render_template("wallet/debit.html", wallets=wallets)

    saved = record_debit(request.form)

    if "continue" not in request.form:
        return render_template("wallet/index.html", debit_saved=saved)

    return render_template("wallet/debit.html", wallets=walletsi, debit_saved=saved)


@bp.route("/balance/details/<wallet_id>", methods=["GET"])
@login_required
def balance_details(wallet_id):
    charges = Credit.query.filter_by(wallet_id=wallet_id).all()
    for c in charges:
        c.type = "credit"

    debits = Debit.query.filter_by(wallet_id=wallet_id).all()
    for d in debits:
        d.type = "debit"

    operations = debits + charges
    sorted(operations, key=lambda op: op.created_at)

    return render_template("wallet/balance_details.html", operations=operations)


@bp.route("/balance", methods=["GET"])
@login_required
def balance():
    types = balance_overview()
    return render_template("wallet/balance.html", types=types)


@bp.route("/operation/<op_type>/<op_id>", methods=["GET"])
@login_required
def operation(op_type, op_id):
    models = {"credit": Credit, "debit": Debit}

    operation = models[op_type].query.get(op_id)
    details = operation.__dict__
    return render_template("wallet/operation.html", operation=details)


@bp.route("/wallet", methods=("GET", "POST", "DELETE"))
@login_required
def wallet():
    added = -1
    if "delete" in request.form:  # TODO proper deletion
        Wallet.query.get(request.form["delete"]).delete()
    elif request.method == "POST":
        added = add_wallet(request.form)

    accts = get_accts()
    accts["added"] = added
    return render_template("wallet/wallet.html", **accts)
