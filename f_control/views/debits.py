from datetime import datetime

from f_control.models import Debit, Wallet

EXPECTED_FORMAT = "%Y %m %d %H:%M:%S"


def record_debit(req):
    details = {**req}

    if details["created_at"] == "0":
        details["created_at"] = datetime.now()
    else:
        details["created_at"] = datetime.strptime(
            details["created_at"], EXPECTED_FORMAT
        )

    debit = Debit(**details)
    debit.save()

    wallet = Wallet.query.get(details["wallet_id"])
    wallet.debit(debit.amount)

    return True
