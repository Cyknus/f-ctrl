from datetime import datetime, timedelta

from f_control.models import Wallet, CreditAcct
from f_control.config import REFRESH_THRESHOLD

THRESHOLD = timedelta(minutes=REFRESH_THRESHOLD)


def balance_overview():
    debit = Wallet.query.filter_by(acct_type="debit").all()
    cash = Wallet.query.filter_by(acct_type="cash").all()
    credit = CreditAcct.query.join(CreditAcct.wallet).all()
    for c in credit:
        c.id = c.wallet.id
        c.balance = c.wallet.balance
        c.name = c.wallet.name
        c.to_pay = c.next_payment()
        c.available = c.credit_limit - c.wallet.balance
    types = {"credit": credit, "debit": debit, "cash": cash}
    return types
