from datetime import datetime

from f_control.models import Credit, Wallet, Subcategory

EXPECTED_FORMAT = "%Y %m %d %H:%M:%S"


def record_charge(req):
    details = {**req}

    subs = ""
    for k in details.keys():
        if "subs." in k:
            subs += k[5:]

    subs = ",".join(subs)
    details["subs"] = subs

    if details["created_at"] == "0":
        details["created_at"] = datetime.now()
    else:
        details["created_at"] = datetime.strptime(
            details["created_at"], EXPECTED_FORMAT
        )

    credit = Credit(**details)
    credit.save()

    wallet = Wallet.query.get(details["wallet_id"])
    wallet.credit(credit.amount)

    return True


def get_charge_opts():
    wallets = Wallet.query.all()
    subs = Subcategory.query.all()
    categories = list(set([c for s in subs for c in s.category.split(",")]))
    return {"wallets": wallets, "subs": subs, "categories": categories}
