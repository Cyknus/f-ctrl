import time
from datetime import datetime
import atexit

from apscheduler.schedulers.background import BackgroundScheduler

from f_control.models import Wallet, CreditAcct, DebitAcct, Cash, DailyBalance


def add_wallet(req):
    details = {**req}
    new_wallet_id = create_wallet(**details)
    details["wallet_id"] = new_wallet_id

    w_func = {
        "credit": create_credit_acct,
        "debit": create_debit_acct,
        "cash": create_cash_wallet,
    }
    w_type = details["w_type"]
    return w_func[w_type](**details)


def create_credit_acct(
    wallet_id, closure_day, interest_rate, credit_limit, yearly_fee, yf_date, **kwargs
):
    yf_date = datetime.strptime(yf_date, "%m")
    credit_acct = CreditAcct(
        closure_day, credit_limit, interest_rate, yearly_fee, yf_date, wallet_id
    )
    credit_acct.save()

    daily_balance = DailyBalance(kwargs["balance"], credit_acct.id)
    daily_balance.save()

    return credit_acct.id


def create_debit_acct(wallet_id, interest_rate, is_savings, **kwargs):
    debit_acct = DebitAcct(is_savings, interest_rate, wallet_id)
    debit_acct.save()
    return debit_acct.id


def create_cash_wallet(wallet_id, **kwargs):
    cash = Cash(wallet_id)
    cash.save()
    return cash.id


def create_wallet(name, balance, w_type, **kwargs):
    updated_at = datetime.now()
    wallet = Wallet(name, balance, w_type, updated_at)
    wallet.save()
    return wallet.id


def get_accts():
    cash = Cash.query.join(Cash.wallet).all()
    c_acct = CreditAcct.query.join(CreditAcct.wallet).all()
    d_acct = DebitAcct.query.join(DebitAcct.wallet).all()
    return {"c_acct": c_acct, "d_acct": d_acct, "cash": cash}


#######################################################################
############### Daily balance scheduled insertion #####################
#######################################################################


def insert_daily_balance():
    c_acct = CreditAcct.query.join(CreditAcct.wallet).all()
    for ca in c_acct:
        daily_balance = DailyBalance(ca.wallet.balance, ca.id)
        daily_balance.save()


scheduler = BackgroundScheduler()
scheduler.add_job(insert_daily_balance, "cron", hour=0, second=2)
scheduler.start()
atexit.register(lambda: scheduler.shutdown())
