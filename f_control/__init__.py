import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy as SQLA

from . import auth, wallet
from f_control.models import db


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        SQLALCHEMY_DATABASE_URI=os.environ["DBURL"],
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    db.create_all(app=app)

    app.register_blueprint(auth.bp)
    app.register_blueprint(wallet.bp)
    app.add_url_rule("/", endpoint="index")

    return app
